import requests
from bs4 import BeautifulSoup
from time import sleep
from glob import glob
import csv
import json
from neo4jrestclient.client import GraphDatabase
from neo4jrestclient import client
from PIL import Image

from urllib.request import urlretrieve

# The six degrees of Civis
# One intern's web scraping journey

def get_seed_page():

    with open("seed.html", 'r') as infile:
        return infile.read()

def all_except_bio():

    bs_obj = BeautifulSoup(get_seed_page(), 'lxml')

    prefix = 'https://civisanalytics.com'


    people = bs_obj.select("#team-isotope > a")

    teams = [p.select_one("div.item").get("class")[1] for p in people]


    hrefs = [person.get('href') for person in people]

    img_tag = [person.select_one("img") for person in people]

    img_urls = [img.get('src') for img in img_tag]
    names = [img.get('alt') for img in img_tag]

    roles = [p.select_one("div.position-team").text for p in people]


    return [(name, href, team, role, img_url) for name, href, team, role, img_url in
            zip(names, hrefs, teams, roles, img_urls)]

def biographies():

    files = glob('html/*')

    def extract(fp):

        with open(fp, 'r') as infile:
            bs_obj = BeautifulSoup(infile.read(), 'lxml')

        info_div = bs_obj.select("body > div.light-content.no-padding > div > div > div.pure-u-1.pure-u-md-1-2")[0]

        return ' '.join([p.text for p in info_div.select("div.bio-content > p")])

    return [extract(fp) for fp in files]

def combine():

    all_else = all_except_bio()
    bios = biographies()

    with open("data.csv", 'w') as csv_out:
        writer = csv.writer(csv_out)
        writer.writerow(["id", "name", "href", "team", "role", "img_url"])

        for i, (data_item, bio) in enumerate(zip(all_else, bios)):

            name, href, team, role, img_url = data_item
            team = {
                "ds": "Data Science",
                "tech": "Engineering",
                "bdcs": "Business Development and Client Success",
                "ops": "Operation",
                "media": "Media Science",
                "engage": "Applied Data Science",
                "dr": "Data Research"
            }[team]


            writer.writerow([i, name, href, team, role, img_url])
            with open("profiles/{}".format(i), "w") as outfile:
                outfile.write(bio)

def as_one_file():

    files = glob('profiles/*')

    def get_text(fp):

        with open(fp, 'r') as infile:
            return infile.read().split("\n", 1)[1:][0]

    data = ' '.join([get_text(_file) for _file in files])

    with open("total.txt", 'w') as outfile:
        outfile.write(data)
def download_pictures():

    with open("data.csv", "r") as infile:
        next(infile)

        for line in infile:

            _id, name, href, team, role, img_url = line.split(",")

            url = "https://civisanalytics.com{}".format(img_url)
            print (_id)
            urlretrieve(url, "jpg/{}".format(_id))
            sleep(1)
def con():

    for i in range(0,101):
        im = Image.open("jpg/{}".format(i))
        im.save("png/{}.png".format(i),"PNG")

def extract_entities():

    with open("entities.json", "r") as infile:
        jsons = json.load(infile)

    names = [x[0] for x in all_except_bio()]

    es = [x.get("entities") for x in jsons]

    entities_return = []

    for name, en_list in zip(names, es):

        first, last = name.split(" ", 1)

        black_set = set([first, last, "Civis", "Civis Analytics", name])

        entities_return.append([thing for thing in en_list if thing.get("text") not in black_set])

    return entities_return

def labels_list():

    names = []

    db = GraphDatabase("http://localhost:7474", username="neo4j", password="mypassword")


    teams = [ "Data Science","Engineering","Business Development and Client Success", "Operation", "Media Science","Applied Data Science","Data Research"]

    for t in teams:

        q = """MERGE (n:Team {{name: "{name}"}})""".format(name = t)
        results = db.query(q, data_contents = True)


    with open("data.csv", "r") as infile:
        next(infile)

        for line in infile:

            _id, name, href, team, role, img_url = line.split(",")

            q = """MERGE (n:Person {{name: "{name}"}}) ON CREATE SET n.url = "{url}", n.role = "{role}", n.team = "{team}", n._id = "{_id}" """.format(name=name,
                                                                                                                                    url = 'img/{}.png'.format(_id),
                                                                                                                                   _id = _id,
                                                                                                                                   role = role,
                                                                                                                                    team= team)
            results = db.query(q, data_contents=True)

            q = """MATCH (p:Person {{_id:"{_id}"}}), (r:Team {{name:"{name}"}}) CREATE (p)-[:ASSOC]->(r) """.format(_id = _id,name=team)

            results = db.query(q, data_contents = True)
            print (q)

    e = extract_entities()

    for index, e_list in enumerate(e):
        for entity in e_list:

            name = entity.get("text")
            label = entity.get("type")

            q = """MERGE (n:{label} {{name: "{name}"}}) """.format(label=label,name=name)

            results = db.query(q, data_contents = True)

            q = """MATCH (p:Person {{_id:"{_id}"}}), (r:{label} {{name:"{name}"}}) CREATE (p)-[:ASSOC]->(r) """.format(_id = index,name=name, label=label)

            results = db.query(q, data_contents = True)

labels_list()

# with open("data.csv", "r") as infile:
# people = [x.split(',')[1] for x in infile]

# x = people[1:]

"""
db = GraphDatabase("http://localhost:7474", username="neo4j", password="mypassword")

user = db.labels.create("UserFUCKINGFUC K")
civis = db.labels.create("Employee")
u1 = db.nodes.create(name="Marco")
user.add(u1)
u2 = db.nodes.create(name="Daniela")
user.add(u2)

beer = db.labels.create("Beer")
b1 = db.nodes.create(name="Punk IPA")
b2 = db.nodes.create(name="Hoegaarden Rosee")
# You can associate a label with many nodes in one go
beer.add(b1, b2)
"""
